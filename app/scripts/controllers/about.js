'use strict';

/**
 * @ngdoc function
 * @name developmentApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the developmentApp
 */
angular.module('developmentApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
